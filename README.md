# University Management System

## Technologies Used
- IntalliJ IDEA
- JDK - version 11
- Postgresql - version latest
- Apache-Tomcat - version 8
- RabbitMQ-Server - version latest
- Liquibase
- Docker

## Libraries Used
- Apache POI
- Flying saucer pdf library
- Thymeleaf
- ModelMapper
- Java mail sender
- SpringDoc Openapi UI (Swagger 3)

## Setup Postgresql and RabbitMQ

To setup Postgresql and RabbitMQ install Docker and in the root directory
open terminal and run following command:
```bash
Docker compose up -d
```
This runs postgresql container on ```localhost:5434``` and RabbitMq
on ```localhost:5672```

## Use cases

**RabbitMQ** - used to do some background operations. For example, 
it is used to send email messages to user's email

**Liquibase** - database migration tool, used to manage database schemas and create default initial data for tables

**Apache POI** - used to create excel file

**Thymeleaf + Flying saucer pdf library** - used to generate pdf files

**Java mail sender** - used to send emails to user's

**Docker** - used to create server containers such as Postgresql, RabbitMQ

**ModelMapper** - used to map entity classes to DTO classes and vice verca

**OpenAPI(Swagger 3)** - used to document spring boot application

## API documentation

You can find API documentation on postman or Swagger, for swagger use this link
```http://localhost:8084/swagger-ui/index.html```
