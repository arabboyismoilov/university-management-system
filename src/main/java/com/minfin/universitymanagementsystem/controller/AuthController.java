package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.auth.LoginDto;
import com.minfin.universitymanagementsystem.dto.auth.LoginResponseDto;
import com.minfin.universitymanagementsystem.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Tag(name = "Auth")
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/auth")
public class AuthController {
    private final AuthService authService;

    @Operation(summary = "Login")
    @PostMapping("login")
    public ResponseEntity<LoginResponseDto> login(@Valid @RequestBody LoginDto loginDto){
        LoginResponseDto login = authService.login(loginDto);
        return ResponseEntity.ok(login);
    }

    @Operation(summary = "Refresh token")
    @GetMapping("refresh")
    public ResponseEntity<LoginResponseDto> refreshToken(HttpServletRequest request){
        LoginResponseDto login = authService.refreshToken(request);
        return ResponseEntity.ok(login);
    }
}
