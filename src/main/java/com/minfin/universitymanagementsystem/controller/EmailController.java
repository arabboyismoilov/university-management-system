package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.email.CustomEmailDto;
import com.minfin.universitymanagementsystem.dto.email.EmailDto;
import com.minfin.universitymanagementsystem.service.EmailService;
import com.minfin.universitymanagementsystem.service.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.minfin.universitymanagementsystem.enums.SuccessMessages.EMAIL_SENT;

@Tag(name = "Email")
@Validated
@RestController
@RequestMapping("/api/v1/mail")
@RequiredArgsConstructor
public class EmailController {
    private final EmailService emailService;
    private final StudentService studentService;

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Send email to any person", description = "You can send email to any person")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping
    public ResponseEntity<String> sendEmail(@RequestBody @Valid EmailDto emailDto){
        emailService.sendEmail(emailDto);
        return ResponseEntity.ok(EMAIL_SENT.getMessage());
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Send email to student", description = "You can send email to student with attachment or without")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping("/student/{studentId}")
    public ResponseEntity<String> sendEmailToStudent(
            @PathVariable Long studentId,
            @RequestBody @Valid CustomEmailDto customEmailDto
    ){
        studentService.sendEmailToStudent(customEmailDto, studentId);
        return ResponseEntity.ok(EMAIL_SENT.getMessage());
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Send email to group of students", description = "You can send email to group of students with attachment or without")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping("/group/{groupId}")
    public ResponseEntity<String> sendEmailToGroupOfStudents(
            @RequestBody @Valid CustomEmailDto customEmailDto,
            @PathVariable Long groupId
    ){
        studentService.sendEmailToGroupOfStudents(customEmailDto, groupId);
        return ResponseEntity.ok(EMAIL_SENT.getMessage());
    }
}
