package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.exam.ExamCreateDto;
import com.minfin.universitymanagementsystem.dto.exam.ExamEditDto;
import com.minfin.universitymanagementsystem.dto.exam.ExamResponseDto;
import com.minfin.universitymanagementsystem.service.ExamService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.SuccessMessages.EXAM_DELETED;

@Tag(name = "Exam")
@Validated
@RestController
@RequestMapping("/api/v1/exam")
@RequiredArgsConstructor
public class ExamController {
    private final ExamService examService;

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Get all exams", description = "Retrieves all exams in pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping
    public ResponseEntity<List<ExamResponseDto>> getAllExams(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        return ResponseEntity.ok(examService.getAllExams(pageNumber, pageSize));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Get exams by subjectId", description = "Retrieves exams by subjectId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/subject/{subjectId}")
    public ResponseEntity<List<ExamResponseDto>> getExamsBySubjectId(@PathVariable Long subjectId){
        return ResponseEntity.ok(examService.getExamsBySubjectId(subjectId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get exam", description = "Retrieves an exam based on given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Exam is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/{examId}")
    public ResponseEntity<ExamResponseDto> getExam(@PathVariable Long examId){
        return ResponseEntity.ok(examService.getExam(examId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Create exam", description = "Creates a new exam")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Given dto is not valid"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping
    public ResponseEntity<ExamResponseDto> createExam(@Valid @RequestBody ExamCreateDto examCreateDto){
        return ResponseEntity.ok(examService.createExam(examCreateDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Edit exam", description = "Edit an exam")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Exam is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PutMapping("/{examId}")
    public ResponseEntity<ExamResponseDto> editExam(@PathVariable Long examId, @RequestBody ExamEditDto examEditDto){
        return ResponseEntity.ok(examService.editExam(examId, examEditDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Delete exam", description = "Delete an exam")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Exam is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @DeleteMapping("/{examId}")
    public ResponseEntity<String> deleteExam(@PathVariable Long examId){
        examService.deleteExam(examId);
        return ResponseEntity.ok(String.format(EXAM_DELETED.getMessage(), examId));
    }
}
