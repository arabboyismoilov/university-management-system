package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.faculty.FacultyCreateDto;
import com.minfin.universitymanagementsystem.dto.faculty.FacultyEditDto;
import com.minfin.universitymanagementsystem.dto.faculty.FacultyResponseDto;
import com.minfin.universitymanagementsystem.service.FacultyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.SuccessMessages.FACULTY_DELETED;

@Tag(name = "Faculty")
@Validated
@RestController
@RequestMapping("/api/v1/faculty")
@RequiredArgsConstructor
public class FacultyController {
    private final FacultyService facultyService;


    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Get all faculties", description = "Retrieves all faculties in pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping
    public ResponseEntity<List<FacultyResponseDto>> getAllFaculties(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        return ResponseEntity.ok(facultyService.getAllFaculties(pageNumber, pageSize));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get faculty", description = "Retrieves a faculty based on given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Faculty is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/{facultyId}")
    public ResponseEntity<FacultyResponseDto> getFaculty(@PathVariable Long facultyId){
        return ResponseEntity.ok(facultyService.getFaculty(facultyId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Create faculty", description = "Creates a new faculty")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Given dto is not valid"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping
    public ResponseEntity<FacultyResponseDto> createFaculty(@Valid @RequestBody FacultyCreateDto facultyCreateDto){
        return ResponseEntity.ok(facultyService.createFaculty(facultyCreateDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Edit faculty", description = "Edit a faculty")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Faculty is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PutMapping("/{facultyId}")
    public ResponseEntity<FacultyResponseDto> editFaculty(@PathVariable Long facultyId, @RequestBody FacultyEditDto facultyEditDto){
        return ResponseEntity.ok(facultyService.editFaculty(facultyId, facultyEditDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Delete faculty", description = "Delete a faculty")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Faculty is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @DeleteMapping("/{facultyId}")
    public ResponseEntity<String> deleteFaculty(@PathVariable Long facultyId){
        facultyService.deleteFaculty(facultyId);
        return ResponseEntity.ok(String.format(FACULTY_DELETED.getMessage(), facultyId));
    }
}
