package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.group.GroupCreateDto;
import com.minfin.universitymanagementsystem.dto.group.GroupEditDto;
import com.minfin.universitymanagementsystem.dto.group.GroupResponseDto;
import com.minfin.universitymanagementsystem.dto.group.GroupSubjectAddDto;
import com.minfin.universitymanagementsystem.service.GroupService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.SuccessMessages.GROUP_DELETED;

@Tag(name = "Group")
@Validated
@RestController
@RequestMapping("/api/v1/group")
@RequiredArgsConstructor
public class GroupController {
    private final GroupService groupService;

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Get all groups", description = "Retrieves all groups in pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping
    public ResponseEntity<List<GroupResponseDto>> getAllGroups(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        return ResponseEntity.ok(groupService.getAllGroups(pageNumber, pageSize));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get group", description = "Retrieves a group based on given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Group is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/{groupId}")
    public ResponseEntity<GroupResponseDto> getGroup(@PathVariable Long groupId){
        return ResponseEntity.ok(groupService.getGroup(groupId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get groups in a faculty", description = "Retrieves groups based on given facultyId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/faculty/{facultyId}")
    public ResponseEntity<List<GroupResponseDto>> getFacultyGroups(@PathVariable Long facultyId){
        return ResponseEntity.ok(groupService.getGroupsByFacultyId(facultyId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Create group", description = "Creates a new group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Given dto is not valid"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping
    public ResponseEntity<GroupResponseDto> createGroup(@Valid @RequestBody GroupCreateDto groupCreateDto){
        return ResponseEntity.ok(groupService.createGroup(groupCreateDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Add subject to group", description = "Adds subject to group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Given dto is not valid"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping("/addsubject")
    public ResponseEntity<GroupResponseDto> addSubjectToGroup(@Valid @RequestBody GroupSubjectAddDto groupSubjectAddDto){
        return ResponseEntity.ok(groupService.addSubjectToGroup(groupSubjectAddDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Edit group", description = "Edit a group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Group is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PutMapping("/{facultyId}")
    public ResponseEntity<GroupResponseDto> editGroup(@PathVariable Long facultyId, @RequestBody GroupEditDto groupEditDto){
        return ResponseEntity.ok(groupService.editGroup(facultyId, groupEditDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Delete group", description = "Delete a group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Group is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @DeleteMapping("/{groupId}")
    public ResponseEntity<String> deleteGroup(@PathVariable Long groupId){
        groupService.deleteGroup(groupId);
        return ResponseEntity.ok(String.format(GROUP_DELETED.getMessage(), groupId));
    }
}
