package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.mark.MarkEditDto;
import com.minfin.universitymanagementsystem.dto.mark.MarkResponseDto;
import com.minfin.universitymanagementsystem.dto.mark.MarkCreateDto;
import com.minfin.universitymanagementsystem.service.MarkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.SuccessMessages.EXAM_MARKS_DELETED;
import static com.minfin.universitymanagementsystem.enums.SuccessMessages.MARK_DELETED;

@Tag(name = "Mark")
@Validated
@RestController
@RequestMapping("/api/v1/mark")
@RequiredArgsConstructor
public class MarkController {
    private final MarkService markService;

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Get all marks", description = "Retrieves all marks in pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping
    public ResponseEntity<List<MarkResponseDto>> getAllMarks(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        return ResponseEntity.ok(markService.getAllMarks(pageNumber, pageSize));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get marks for exam in group", description = "Retrieves all marks according groupId and examId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/group/{groupId}")
    public ResponseEntity<List<MarkResponseDto>> getGroupExamMarks(
            @PathVariable Long groupId,
            @RequestParam Long examId
    ){
        return ResponseEntity.ok(markService.getMarksByGroupIdAndExamId(groupId, examId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Generate excel file for marks of exam for group", description = "Retrieves excel file contains all marks according groupId and examId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping(value = "file/group/{groupId}")
    public ResponseEntity<ByteArrayResource> generateGroupExamMarks(
            @PathVariable Long groupId,
            @RequestParam Long examId
    ){

        // Set content type and headers for the response
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        headers.setContentDisposition(ContentDisposition
                .attachment()
                .filename(String.format("marks-groupId-%s-examId-%s.xlsx", groupId, examId))
                .build());

        // generating file
        byte[] data = markService.generateFileForExamMarksForGroup(groupId, examId);
        ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                .headers(headers)
                .body(resource);
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Get mark", description = "Retrieves a mark based on given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Mark is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/{markId}")
    public ResponseEntity<MarkResponseDto> getMark(@PathVariable Long markId){
        return ResponseEntity.ok(markService.getMark(markId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Create mark", description = "Creates a new mark")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Given dto is not valid"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping
    public ResponseEntity<MarkResponseDto> createMark(@Valid @RequestBody MarkCreateDto markCreateDto){
        return ResponseEntity.ok(markService.createMark(markCreateDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Edit mark", description = "Edit a mark")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Mark is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PutMapping("/{markId}")
    public ResponseEntity<MarkResponseDto> editMark(@PathVariable Long markId, @RequestBody MarkEditDto markEditDto){
        return ResponseEntity.ok(markService.editMark(markId, markEditDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Delete mark", description = "Delete a mark")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Mark is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @DeleteMapping("/{markId}")
    public ResponseEntity<String> deleteMark(@PathVariable Long markId){
        markService.deleteMark(markId);
        return ResponseEntity.ok(String.format(MARK_DELETED.getMessage(), markId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Delete marks by examId", description = "Delete all marks by examId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Exam is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @DeleteMapping("/exam/{examId}")
    public ResponseEntity<String> deleteExamMarks(@PathVariable Long examId){
        markService.deleteMarlByExamId(examId);
        return ResponseEntity.ok(String.format(EXAM_MARKS_DELETED.getMessage(), examId));
    }
}
