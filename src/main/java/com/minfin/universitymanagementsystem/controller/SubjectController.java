package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.subject.SubjectCreateDto;
import com.minfin.universitymanagementsystem.dto.subject.SubjectEditDto;
import com.minfin.universitymanagementsystem.dto.subject.SubjectResponseDto;
import com.minfin.universitymanagementsystem.service.SubjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.SuccessMessages.SUBJECT_DELETED;

@Tag(name = "Subject")
@Validated
@RestController
@RequestMapping("/api/v1/subject")
@RequiredArgsConstructor
public class SubjectController {
    private final SubjectService subjectService;

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get all subjects", description = "Retrieves all subjects in pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping
    public ResponseEntity<List<SubjectResponseDto>> getAllSubjects(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        return ResponseEntity.ok(subjectService.getAllSubjects(pageNumber, pageSize));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get subject", description = "Retrieves a subject based on given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Subject is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/{subjectId}")
    public ResponseEntity<SubjectResponseDto> getSubject(@PathVariable Long subjectId){
        return ResponseEntity.ok(subjectService.getSubject(subjectId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get student subjects", description = "Retrieves a students subjects based on given studentId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/student/{studentId}")
    public ResponseEntity<List<SubjectResponseDto>> getStudentSubjects(@PathVariable Long studentId){
        return ResponseEntity.ok(subjectService.getSubjectsByStudentId(studentId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get group subjects", description = "Retrieves group subjects based on given groupId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/group/{groupId}")
    public ResponseEntity<List<SubjectResponseDto>> getGroupSubjects(@PathVariable Long groupId){
        return ResponseEntity.ok(subjectService.getSubjectsByGroupId(groupId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Create subject", description = "Creates a new subject")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Given dto is not valid"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping
    public ResponseEntity<SubjectResponseDto> createSubject(@Valid @RequestBody SubjectCreateDto subjectCreateDto){
        return ResponseEntity.ok(subjectService.createSubject(subjectCreateDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Edit subject", description = "Edit a subject")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Subject is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PutMapping("/{subjectId}")
    public ResponseEntity<SubjectResponseDto> editSubject(@PathVariable Long subjectId, @RequestBody SubjectEditDto subjectEditDto){
        return ResponseEntity.ok(subjectService.editSubject(subjectId, subjectEditDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Delete subject", description = "Delete a subject")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Subject is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @DeleteMapping("/{subjectId}")
    public ResponseEntity<String> deleteSubject(@PathVariable Long subjectId){
        subjectService.deleteSubject(subjectId);
        return ResponseEntity.ok(String.format(SUBJECT_DELETED.getMessage(), subjectId));
    }
}
