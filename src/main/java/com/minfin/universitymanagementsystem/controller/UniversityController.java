package com.minfin.universitymanagementsystem.controller;

import com.minfin.universitymanagementsystem.dto.university.UniversityCreateDto;
import com.minfin.universitymanagementsystem.dto.university.UniversityEditDto;
import com.minfin.universitymanagementsystem.dto.university.UniversityResponseDto;
import com.minfin.universitymanagementsystem.service.UniversityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.SuccessMessages.UNIVERSITY_DELETED;

@Tag(name = "University")
@Validated
@RestController
@RequestMapping("/api/v1/university")
@RequiredArgsConstructor
public class UniversityController {
    private final UniversityService universityService;

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Get all universities", description = "Retrieves all universities in pagination")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping
    public ResponseEntity<List<UniversityResponseDto>> getAllUniversities(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize){
        return ResponseEntity.ok(universityService.getAllUniversities(pageNumber, pageSize));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_STUDENT', 'ROLE_TEACHER')")
    @Operation(summary = "Get university", description = "Retrieves a university based on given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "University is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @GetMapping("/{universityId}")
    public ResponseEntity<UniversityResponseDto> getUniversity(@PathVariable Long universityId){
        return ResponseEntity.ok(universityService.getUniversity(universityId));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Create university", description = "Creates a new university")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Given dto is not valid"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PostMapping
    public ResponseEntity<UniversityResponseDto> createUniversity(@Valid @RequestBody UniversityCreateDto universityCreateDto){
        return ResponseEntity.ok(universityService.createUniversity(universityCreateDto));
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Edit university", description = "Edit a university")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "University is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @PutMapping("/{universityId}")
    public ResponseEntity<UniversityResponseDto> editUniversity(@PathVariable Long universityId, @RequestBody UniversityEditDto universityEditDto){
        return ResponseEntity.ok(universityService.editUniversity(universityId, universityEditDto));
    }


    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @Operation(summary = "Delete university", description = "Delete a university")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "University is not found"),
            @ApiResponse(responseCode = "500", description = "Internal error")
    })
    @DeleteMapping("/{universityId}")
    public ResponseEntity<String> deleteUniversity(@PathVariable Long universityId){
        universityService.deleteUniversity(universityId);
        return ResponseEntity.ok(String.format(UNIVERSITY_DELETED.getMessage(), universityId));
    }
}
