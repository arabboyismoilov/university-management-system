package com.minfin.universitymanagementsystem.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoginDto {
    @NotBlank(message = "email cannot be null")
    @Email(message = "email should be in proper format")
    private String email;

    @NotBlank(message = "password cannot be null")
    @Size(min = 4, message = "password should be at least 4 characters")
    private String password;
}
