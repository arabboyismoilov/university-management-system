package com.minfin.universitymanagementsystem.dto.email;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmailDto {
    @NotBlank(message = "to field cannot be null")
    private String to;

    @NotBlank(message = "subject field cannot be null")
    private String subject;

    @NotBlank(message = "body field cannot be null")
    private String body;
}
