package com.minfin.universitymanagementsystem.dto.email;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmailMessage {
    private EmailDto emailDto;
    private String filePath;

    public EmailMessage(EmailDto emailDto) {
        this.emailDto = emailDto;
    }
}
