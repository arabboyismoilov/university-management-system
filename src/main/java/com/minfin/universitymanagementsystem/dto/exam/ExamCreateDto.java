package com.minfin.universitymanagementsystem.dto.exam;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExamCreateDto {
    @NotBlank(message = "name field cannot be blank or null")
    private String name;

    @NotNull(message = "subjectId field cannot be null")
    private Long subjectId;
}
