package com.minfin.universitymanagementsystem.dto.exam;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExamResponseDto {
    private Long id;
    private String name;
    private Long subjectId;
    private String createdAt;
}
