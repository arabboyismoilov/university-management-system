package com.minfin.universitymanagementsystem.dto.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExceptionResponseDto {
    private int status;
    private String message;
}
