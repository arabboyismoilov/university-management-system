package com.minfin.universitymanagementsystem.dto.faculty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FacultyCreateDto {
    @NotBlank(message = "name field cannot be null or blank")
    private String name;

    @NotNull(message = "universityId field cannot be null")
    private Long universityId;
}
