package com.minfin.universitymanagementsystem.dto.faculty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FacultyEditDto {
    private String name;

    private Long universityId;
}
