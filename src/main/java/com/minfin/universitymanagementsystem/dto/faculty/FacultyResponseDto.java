package com.minfin.universitymanagementsystem.dto.faculty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FacultyResponseDto {
    private Long id;
    private String name;
    private Long universityId;
    private String createdAt;
}
