package com.minfin.universitymanagementsystem.dto.group;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GroupCreateDto {
    @NotBlank(message = "name field cannot be blank or null")
    private String name;

    @NotNull(message = "openedYear cannot be null")
    private Integer openedYear;

    @NotNull(message = "facultyId cannot be null")
    private Long facultyId;
}
