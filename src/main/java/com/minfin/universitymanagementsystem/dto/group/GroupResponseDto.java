package com.minfin.universitymanagementsystem.dto.group;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GroupResponseDto {
    private Long id;
    private String name;
    private Integer openedYear;
    private Long facultyId;
    private Integer numberOfStudents;
    private Integer numberOfSubjects;
    private String createdAt;
}
