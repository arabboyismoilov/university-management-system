package com.minfin.universitymanagementsystem.dto.group;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GroupSubjectAddDto {
    private Long subjectId;
    private Long groupId;
}
