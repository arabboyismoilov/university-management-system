package com.minfin.universitymanagementsystem.dto.mark;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MarkCreateDto {
    @NotNull(message = "value field cannot be null")
    @Max(value = 100, message = "value should be maximum at 100")
    @Min(value = 0, message = "value should be at least 0")
    private Integer value; // mark value from 0 to 100

    @NotNull(message = "studentId field cannot be null")
    private Long studentId;

    @NotNull(message = "examId field cannot be null")
    private Long examId;
}
