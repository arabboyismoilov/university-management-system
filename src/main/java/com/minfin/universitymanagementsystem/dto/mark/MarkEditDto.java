package com.minfin.universitymanagementsystem.dto.mark;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MarkEditDto {
    private Integer value;
}
