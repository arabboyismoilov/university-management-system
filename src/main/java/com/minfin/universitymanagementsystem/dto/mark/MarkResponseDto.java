package com.minfin.universitymanagementsystem.dto.mark;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MarkResponseDto {
    private Long id;
    private Integer value; // mark value from 0 to 100
    private String studentName;
    private String subjectName;
    private String examName;
    private Long studentId;
    private Long subjectId;
    private Long examId;
    private String createdAt;
}
