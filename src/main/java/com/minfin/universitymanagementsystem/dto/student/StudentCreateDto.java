package com.minfin.universitymanagementsystem.dto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StudentCreateDto {
    @NotBlank(message = "email field cannot be blank or null")
    @Email(message = "email field should be valid email address")
    private String email;

    @NotBlank(message = "password field cannot be blank or null")
    private String password;

    @NotBlank(message = "name field cannot be blank or null")
    private String name;

    @NotNull(message = "groupId field cannot be null")
    private Long groupId;

    private String address;
    private String phoneNumber;
    private String birthDate;
    private Integer enteredYear;
    private String academicYear;
}
