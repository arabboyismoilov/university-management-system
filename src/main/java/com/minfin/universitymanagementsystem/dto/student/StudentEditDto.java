package com.minfin.universitymanagementsystem.dto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StudentEditDto {
    private String name;
    private Long groupId;
    private String address;
    private String phoneNumber;
    private String birthDate;
    private Integer enteredYear;
    private String academicYear;
}
