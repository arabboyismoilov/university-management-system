package com.minfin.universitymanagementsystem.dto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StudentResponseDto {
    private Long id;
    private String name;
    private String address;
    private String birthDate;
    private Integer enteredYear;
    private String academicYear;
    private String groupName;
    private String facultyName;
    private Long userId;
    private Long groupId;
    private Long facultyId;
    private String createdAt;
}
