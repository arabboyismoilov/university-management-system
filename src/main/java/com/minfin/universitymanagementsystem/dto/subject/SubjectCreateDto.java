package com.minfin.universitymanagementsystem.dto.subject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubjectCreateDto {
    @NotBlank(message = "name field cannot be blank")
    private String name;

    @NotNull(message = "hours field cannot be null")
    private Integer hours;

    @NotNull(message = "universityId field cannot be null")
    private Long universityId;
}
