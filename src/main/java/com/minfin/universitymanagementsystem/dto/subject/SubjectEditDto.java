package com.minfin.universitymanagementsystem.dto.subject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubjectEditDto {
    private String name;
    private Integer hours;
}
