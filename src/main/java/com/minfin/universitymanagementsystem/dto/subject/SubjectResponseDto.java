package com.minfin.universitymanagementsystem.dto.subject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubjectResponseDto {
    private Long id;
    private String name;
    private Integer hours;
    private Long universityId;
    private String createdAt;
}
