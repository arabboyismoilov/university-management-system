package com.minfin.universitymanagementsystem.dto.university;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UniversityCreateDto {
    @NotBlank(message = "name cannot be blank or null")
    private String name;

    @NotBlank(message = "address cannot be blank or null")
    private String address;

    @NotNull
    @Min(value = 1, message = "year cannot be negative number")
    private Integer openedYear;
}
