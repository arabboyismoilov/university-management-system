package com.minfin.universitymanagementsystem.dto.university;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UniversityEditDto {
    private String name;

    private String address;

    private Integer openedYear;
}
