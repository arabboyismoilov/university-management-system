package com.minfin.universitymanagementsystem.dto.university;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UniversityResponseDto {
    private Long id;
    private String name;
    private String address;
    private Integer openedYear;
    private String createdAt;
}
