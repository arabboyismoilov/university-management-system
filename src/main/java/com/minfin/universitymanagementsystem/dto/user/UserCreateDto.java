package com.minfin.universitymanagementsystem.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserCreateDto {
    @NotBlank(message = "email cannot be blank")
    @Email(message = "email should be in proper format")
    private String email;

    @NotBlank(message = "email cannot be blank")
    @Size(min = 4, message = "password should be at least 4 characters")
    private String password;

    @NotBlank(message = "name cannot be blank")
    private String name;

    @NotBlank(message = "roleName cannot be blank")
    private String roleName;

    private String phoneNumber;
}
