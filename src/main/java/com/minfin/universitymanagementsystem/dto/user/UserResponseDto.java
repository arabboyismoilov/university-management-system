package com.minfin.universitymanagementsystem.dto.user;

import com.minfin.universitymanagementsystem.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserResponseDto {
    private Long id;
    private String email;
    private String name;
    private String phoneNumber;
    private List<String> roles;
    private String createdAt;
}
