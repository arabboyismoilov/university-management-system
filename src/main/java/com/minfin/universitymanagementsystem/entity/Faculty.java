package com.minfin.universitymanagementsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Faculty extends BaseEntity{
    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "faculty", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<Group> groups;

    @ManyToOne(optional = false)
    @JoinColumn(name = "university_id", referencedColumnName = "id")
    private University university;
}
