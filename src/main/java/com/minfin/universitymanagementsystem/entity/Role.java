package com.minfin.universitymanagementsystem.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Role extends BaseEntity{
    @Column(unique = true)
    private String name;

    @Override
    public String toString() {
        return name;
    }
}