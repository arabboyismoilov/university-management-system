package com.minfin.universitymanagementsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class University extends BaseEntity{
    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private Integer openedYear;

    @OneToMany(mappedBy = "university",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Faculty> facultyList;

    @OneToMany(mappedBy = "university",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Subject> subjects;
}
