package com.minfin.universitymanagementsystem.enums;

public enum EmailMessages {
    SUCCESS_REGISTRATION_SUBJECT("Successful registration"),
    STUDENT_CREATED_MESSAGE(
            "Dear student\n, '%s' university created a STUDENT account for you. \nTo enter it you can use following crediantials, " +
            "please dont forget changing your password after successfully login.\n" +
            "Credentials:\n" +
            "Password: %s\n" +
            "Username: %s\n" +
            "Thanks for your attention. If this is wrong mail, please just forget it!");

    private final String message;

    EmailMessages(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
