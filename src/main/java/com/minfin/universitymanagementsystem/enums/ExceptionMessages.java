package com.minfin.universitymanagementsystem.enums;

public enum ExceptionMessages {
    USER_NOT_FOUND("User is not found, given: %s"),
    REFRESH_TOKEN_NOT_FOUND("Refresh token in header is not found"),
    EMAIL_ALREADY_REGISTERED("Email already registered: %s"),
    UNIVERSITY_NAME_ALREADY_EXIST("University with name [%s] is already exist"),
    UNIVERSITY_NOT_FOUND("University with ID [%s] is not found"),
    FACULTY_ALREADY_EXIST_IN_THIS_UNIVERSITY("Faculty [%s] is already exist in this [%s] university"),
    GROUP_ALREADY_EXIST_IN_THIS_FACULTY("Group [%s] is already exist in this [%s] faculty"),
    SUBJECT_ALREADY_EXIST("Subject [%s] is already exist in university ID [%s]"),
    SUBJECT_ALREADY_EXIST_IN_THIS_GROUP("Subject [%s] is already exist in this group ID [%s]"),
    EXAM_ALREADY_EXIST("Exam [%s] is already exist in Subject ID [%s]"),
    MARK_ALREADY_EXIST("Mark is already exist for student ID [%s] for exam ID[%s]"),
    FACULTY_NOT_FOUND("Faculty with ID [%s] is not found"),
    GROUP_NOT_FOUND("Group with ID [%s] is not found"),
    SUBJECT_NOT_FOUND("Subject with ID [%s] is not found"),
    STUDENT_NOT_FOUND("Student with ID [%s] is not found"),
    EXAM_NOT_FOUND("Exam with ID [%s] is not found"),
    MARK_NOT_FOUND("Mark with ID [%s] is not found"),
    INTERNAL_ERROR("Internal error occurred"),
    INTERNAL_ERROR_FILE_NOT_CREATED("Internal error occurred, file couldn't created"),
    ROLE_NOT_FOUND("Role is not found, given: %s");


    private final String message;

    ExceptionMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
