package com.minfin.universitymanagementsystem.enums;

public enum SuccessMessages {
    EXAM_DELETED("Exam with ID[] %s is deleted"),
    FACULTY_DELETED("Faculty with ID[] %s is deleted"),
    GROUP_DELETED("Group with ID[] %s is deleted"),
    MARK_DELETED("Mark with ID[] %s is deleted"),
    EXAM_MARKS_DELETED("Marks of exam with ID[] %s are deleted"),
    STUDENT_DELETED("Student with ID[] %s is deleted"),
    SUBJECT_DELETED("Subject with ID[] %s is deleted"),
    UNIVERSITY_DELETED("University with ID[] %s is deleted"),
    EMAIL_SENT("Email sent");

    private final String message;

    SuccessMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
