package com.minfin.universitymanagementsystem.exception;

import com.minfin.universitymanagementsystem.exception.config.ApiRequestException;
import org.springframework.http.HttpStatus;

public class BadRequestException extends ApiRequestException {
    public BadRequestException(String message) {
        super(message, HttpStatus.BAD_REQUEST.value());
    }
}
