package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.exam.ExamEditDto;
import com.minfin.universitymanagementsystem.dto.exam.ExamResponseDto;
import com.minfin.universitymanagementsystem.entity.Exam;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ExamMapper {
    private final ModelMapper modelMapper;

    public ExamResponseDto responseDto(Exam exam){
        ExamResponseDto mapped = modelMapper.map(exam, ExamResponseDto.class);
        mapped.setSubjectId(exam.getSubject().getId());
        return mapped;
    }

    public void editExam(Exam exam, ExamEditDto examEditDto){
        modelMapper.map(examEditDto, exam);
    }
}
