package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.faculty.FacultyEditDto;
import com.minfin.universitymanagementsystem.dto.faculty.FacultyResponseDto;
import com.minfin.universitymanagementsystem.entity.Faculty;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FacultyMapper {
    private final ModelMapper modelMapper;

    public FacultyResponseDto responseDto(Faculty faculty){
        FacultyResponseDto mapped = modelMapper.map(faculty, FacultyResponseDto.class);
        mapped.setUniversityId(faculty.getUniversity().getId());
        return mapped;
    }

    public void editFaculty(Faculty faculty, FacultyEditDto facultyEditDto){
        modelMapper.map(facultyEditDto, faculty);
    }
}
