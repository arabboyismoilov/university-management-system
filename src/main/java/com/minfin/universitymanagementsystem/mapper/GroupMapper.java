package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.group.GroupEditDto;
import com.minfin.universitymanagementsystem.dto.group.GroupResponseDto;
import com.minfin.universitymanagementsystem.entity.Group;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GroupMapper {
    private final ModelMapper modelMapper;

    public GroupResponseDto responseDto(Group group){
        GroupResponseDto mapped = modelMapper.map(group, GroupResponseDto.class);
        mapped.setFacultyId(group.getFaculty().getId());

        int numberOfStudents = group.getStudents().size();
        int numberOfSubjects = group.getSubjects().size();
        mapped.setNumberOfStudents(numberOfStudents);
        mapped.setNumberOfSubjects(numberOfSubjects);

        return mapped;
    }

    public void editGroup(Group group, GroupEditDto groupEditDto){
        modelMapper.map(groupEditDto, group);
    }
}
