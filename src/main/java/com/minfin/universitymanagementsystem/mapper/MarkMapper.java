package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.mark.MarkEditDto;
import com.minfin.universitymanagementsystem.dto.mark.MarkResponseDto;
import com.minfin.universitymanagementsystem.entity.Exam;
import com.minfin.universitymanagementsystem.entity.Mark;
import com.minfin.universitymanagementsystem.entity.Student;
import com.minfin.universitymanagementsystem.entity.Subject;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MarkMapper {
    private final ModelMapper modelMapper;

    public MarkResponseDto responseDto(Mark mark){
        MarkResponseDto mapped = modelMapper.map(mark, MarkResponseDto.class);

        Exam exam = mark.getExam();
        Student student = mark.getStudent();
        Subject subject = mark.getSubject();

        mapped.setExamId(exam.getId());
        mapped.setExamName(exam.getName());
        mapped.setStudentId(student.getId());
        mapped.setStudentName(student.getName());
        mapped.setSubjectId(subject.getId());
        mapped.setSubjectName(subject.getName());

        return mapped;
    }

    public void editMark(Mark mark, MarkEditDto markEditDto){
        modelMapper.map(markEditDto, mark);
    }
}
