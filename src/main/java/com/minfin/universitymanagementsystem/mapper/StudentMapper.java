package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.student.StudentEditDto;
import com.minfin.universitymanagementsystem.dto.student.StudentResponseDto;
import com.minfin.universitymanagementsystem.entity.Faculty;
import com.minfin.universitymanagementsystem.entity.Group;
import com.minfin.universitymanagementsystem.entity.Student;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StudentMapper {
    private final ModelMapper modelMapper;

    public StudentResponseDto responseDto(Student student){
        StudentResponseDto mapped = modelMapper.map(student, StudentResponseDto.class);

        Group group = student.getGroup();
        Faculty faculty = group.getFaculty();

        mapped.setGroupId(group.getId());
        mapped.setGroupName(group.getName());

        mapped.setFacultyId(faculty.getId());
        mapped.setFacultyName(faculty.getName());

        mapped.setUserId(student.getUser().getId());
        return mapped;
    }

    public void editStudent(Student student, StudentEditDto studentEditDto){
        PropertyMap<StudentEditDto, Student> skipFieldsMap = new PropertyMap<StudentEditDto, Student>() {
            protected void configure() {
                skip(destination.getId()); // Skip mapping the id field
                skip(destination.getGroup()); // Skip mapping the group field
            }
        };
        modelMapper.addMappings(skipFieldsMap);

        modelMapper.map(studentEditDto, student);
    }
}
