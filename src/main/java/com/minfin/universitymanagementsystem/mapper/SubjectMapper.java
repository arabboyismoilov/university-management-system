package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.subject.SubjectEditDto;
import com.minfin.universitymanagementsystem.dto.subject.SubjectResponseDto;
import com.minfin.universitymanagementsystem.entity.Subject;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SubjectMapper {
    private final ModelMapper modelMapper;

    public SubjectResponseDto responseDto(Subject subject){
        SubjectResponseDto mapped = modelMapper.map(subject, SubjectResponseDto.class);
        mapped.setUniversityId(subject.getUniversity().getId());
        return mapped;
    }

    public void editSubject(Subject subject, SubjectEditDto subjectEditDto){
        modelMapper.map(subjectEditDto, subject);
    }
}
