package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.university.UniversityEditDto;
import com.minfin.universitymanagementsystem.dto.university.UniversityResponseDto;
import com.minfin.universitymanagementsystem.entity.University;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UniversityMapper {
    private final ModelMapper modelMapper;

    public UniversityResponseDto responseDto(University university){
        return modelMapper.map(university, UniversityResponseDto.class);
    }

    public void editUniversity(University university, UniversityEditDto universityEditDto){
        modelMapper.map(universityEditDto, university);
    }
}
