package com.minfin.universitymanagementsystem.mapper;

import com.minfin.universitymanagementsystem.dto.user.UserResponseDto;
import com.minfin.universitymanagementsystem.entity.Role;
import com.minfin.universitymanagementsystem.entity.User;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserMapper {
    private final ModelMapper modelMapper;

    public UserResponseDto entityToResponseDto(User user){
        UserResponseDto responseDto = modelMapper.map(user, UserResponseDto.class);
        responseDto.setRoles(user.getRoles()
                .stream()
                .map(Role::getName)
                .collect(Collectors.toList()));
        return responseDto;
    }
}
