package com.minfin.universitymanagementsystem.rabbitmq.consumer;

import com.minfin.universitymanagementsystem.dto.email.EmailDto;
import com.minfin.universitymanagementsystem.dto.email.EmailMessage;
import com.minfin.universitymanagementsystem.exception.config.ApiRequestException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.io.File;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.INTERNAL_ERROR;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmailSenderConsumer {
    private final JavaMailSender javaMailSender;

    @RabbitListener(queues = "${rabbitmq.queue.email_sender.name}")
    public void consumeOrderDocument(EmailMessage emailMessage){
        log.info(String.format("Received emailDto with email name -> %s", emailMessage.getEmailDto().getTo()));
        if (emailMessage.getFilePath() != null){
            MimeMessage mimeMessage = getMimeMessage(emailMessage.getEmailDto(), emailMessage.getFilePath());
            javaMailSender.send(mimeMessage);
        }
        else {
            SimpleMailMessage simpleMessage = getSimpleMessage(emailMessage.getEmailDto());
            javaMailSender.send(simpleMessage);
        }
    }

    private SimpleMailMessage getSimpleMessage(EmailDto emailDto){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailDto.getTo());
        message.setSubject(emailDto.getSubject());
        message.setText(emailDto.getBody());
        return message;
    }

    private MimeMessage getMimeMessage(EmailDto emailDto, String filePath){
        File attachment = new File(filePath);
        try{
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(emailDto.getTo());
            helper.setSubject(emailDto.getSubject());
            helper.setText(emailDto.getBody());

            // Attach the file
            helper.addAttachment("attachment", attachment);
            return message;
        } catch (MessagingException e) {
            throw new ApiRequestException(INTERNAL_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
