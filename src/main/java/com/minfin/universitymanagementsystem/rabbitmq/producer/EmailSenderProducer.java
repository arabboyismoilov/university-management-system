package com.minfin.universitymanagementsystem.rabbitmq.producer;

import com.minfin.universitymanagementsystem.dto.email.EmailMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailSenderProducer {
    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.queue.email_sender.routing_key}")
    private String routingKey;

    private final RabbitTemplate rabbitTemplate;

    public void sendEmailToQueue(EmailMessage emailMessage){
        log.info(String.format("MailMessage [%s] sent to queue", emailMessage));
        rabbitTemplate.convertAndSend(exchange, routingKey, emailMessage);
    }
}
