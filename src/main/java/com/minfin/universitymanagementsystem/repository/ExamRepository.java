package com.minfin.universitymanagementsystem.repository;

import com.minfin.universitymanagementsystem.entity.Exam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamRepository extends JpaRepository<Exam, Long> {
    boolean existsByNameAndSubject_Id(String name, Long subjectId);
}
