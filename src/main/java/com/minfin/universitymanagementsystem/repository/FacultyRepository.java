package com.minfin.universitymanagementsystem.repository;

import com.minfin.universitymanagementsystem.entity.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacultyRepository extends JpaRepository<Faculty, Long> {
    boolean existsByNameAndUniversity_Id(String name, Long universityId);
}
