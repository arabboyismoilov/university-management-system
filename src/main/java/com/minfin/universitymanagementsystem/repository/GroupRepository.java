package com.minfin.universitymanagementsystem.repository;

import com.minfin.universitymanagementsystem.entity.Faculty;
import com.minfin.universitymanagementsystem.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Long> {
    boolean existsByNameAndFaculty_Id(String name, Long facultyId);
    List<Group> findAllByFaculty(Faculty faculty);
}
