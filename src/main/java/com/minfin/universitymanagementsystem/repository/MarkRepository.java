package com.minfin.universitymanagementsystem.repository;

import com.minfin.universitymanagementsystem.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MarkRepository extends JpaRepository<Mark, Long> {
    boolean existsByStudentAndExam(Student student, Exam exam);
    List<Mark> findAllByStudent_GroupAndExamOrderByValueDesc(Group group, Exam exam);
    void deleteAllByExam(Exam exam);
}
