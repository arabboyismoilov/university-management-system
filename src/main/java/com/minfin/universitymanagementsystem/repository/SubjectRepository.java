package com.minfin.universitymanagementsystem.repository;

import com.minfin.universitymanagementsystem.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject, Long> {
    boolean existsByNameAndUniversity_Id(String name, Long universityId);
}
