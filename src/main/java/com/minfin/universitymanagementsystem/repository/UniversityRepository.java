package com.minfin.universitymanagementsystem.repository;

import com.minfin.universitymanagementsystem.entity.University;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UniversityRepository extends JpaRepository<University, Long> {
    Optional<University> findByName(String name);
    boolean existsByName(String name);
}
