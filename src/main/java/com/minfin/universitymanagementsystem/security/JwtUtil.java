package com.minfin.universitymanagementsystem.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.stream.Collectors;

public abstract class JwtUtil {

    public static String generateAccessToken(UserDetails user){
        long accessTokenExpireTime = 1000 * 60 * 15L*1000; //15 minutes
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+ accessTokenExpireTime))
                .withClaim(
                        "roles",
                        user.getAuthorities().stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList())
                )
                .sign(getAlgorithm());
    }

    public static String generateRefreshToken(UserDetails user){
        long refreshTokenExpireTime = 1000 * 60 * 60 * 24 * 30L; //30 days
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+ refreshTokenExpireTime))
                .sign(getAlgorithm());
    }

    public static Algorithm getAlgorithm(){
        return Algorithm.HMAC256("Secret key".getBytes(StandardCharsets.UTF_8));
    }
}

