package com.minfin.universitymanagementsystem.security;

import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.USER_NOT_FOUND;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig {
    private final UserRepository userRepository;

    @Bean
    public UserDetailsService userDetailsService(){
        return username -> userRepository
                .findByEmail(username).orElseThrow(
                        () -> new NotFoundException(String.format(USER_NOT_FOUND.getMessage(), username))
                );
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
        http.cors().configurationSource(request -> {
            CorsConfiguration corsCfg = new CorsConfiguration();
            corsCfg.addAllowedOriginPattern( "*" );
            corsCfg.addAllowedMethod(CorsConfiguration.ALL);
            return corsCfg.applyPermitDefaultValues();
        });

        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.authorizeRequests().antMatchers(
                "/api/v1/auth/login",
                "/api/v1/auth/refresh",
                "/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**",
                "/swagger-ui/index.html", "/swagger", "/api/v1/users"
        ).permitAll();
        http.authorizeRequests().anyRequest().authenticated();

        http.addFilterBefore(new JwtAuthorizationFilter(userDetailsService()), UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

}
