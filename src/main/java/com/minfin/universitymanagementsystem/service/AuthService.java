package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.auth.LoginDto;
import com.minfin.universitymanagementsystem.dto.auth.LoginResponseDto;

import javax.servlet.http.HttpServletRequest;

public interface AuthService {
    LoginResponseDto login(LoginDto loginDto);
    LoginResponseDto refreshToken(HttpServletRequest request);

}
