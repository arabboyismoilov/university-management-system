package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.email.EmailDto;

public interface EmailService {
    void sendEmail(EmailDto emailDto);
    void sendEmailWithAttachment(EmailDto emailDto, String filePath);
}
