package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.exam.ExamCreateDto;
import com.minfin.universitymanagementsystem.dto.exam.ExamEditDto;
import com.minfin.universitymanagementsystem.dto.exam.ExamResponseDto;

import java.util.List;

public interface ExamService {
    ExamResponseDto createExam(ExamCreateDto examCreateDto);
    List<ExamResponseDto> getAllExams(int pageNumber, int pageSize);
    List<ExamResponseDto> getExamsBySubjectId(Long subjectId);
    ExamResponseDto getExam(Long examId);
    ExamResponseDto editExam(Long examId, ExamEditDto examEditDto);
    void deleteExam(Long examId);
}
