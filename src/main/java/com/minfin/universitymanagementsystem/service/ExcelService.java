package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.mark.MarkResponseDto;

import java.util.List;

public interface ExcelService {
    byte[] generateExamMarksForGroup(List<MarkResponseDto> marks);
}
