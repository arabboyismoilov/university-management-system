package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.faculty.FacultyCreateDto;
import com.minfin.universitymanagementsystem.dto.faculty.FacultyEditDto;
import com.minfin.universitymanagementsystem.dto.faculty.FacultyResponseDto;

import java.util.List;

public interface FacultyService {
    FacultyResponseDto createFaculty(FacultyCreateDto facultyCreateDto);
    List<FacultyResponseDto> getAllFaculties(int pageNumber, int pageSize);
    FacultyResponseDto getFaculty(Long facultyId);
    FacultyResponseDto editFaculty(Long facultyId, FacultyEditDto facultyEditDto);
    void deleteFaculty(Long facultyId);
}
