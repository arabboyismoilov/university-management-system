package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.group.GroupCreateDto;
import com.minfin.universitymanagementsystem.dto.group.GroupEditDto;
import com.minfin.universitymanagementsystem.dto.group.GroupResponseDto;
import com.minfin.universitymanagementsystem.dto.group.GroupSubjectAddDto;

import java.util.List;

public interface GroupService {
    GroupResponseDto createGroup(GroupCreateDto groupCreateDto);
    List<GroupResponseDto> getAllGroups(int pageNumber, int pageSize);
    GroupResponseDto getGroup(Long groupId);
    List<GroupResponseDto> getGroupsByFacultyId(Long facultyId);
    GroupResponseDto editGroup(Long groupId, GroupEditDto groupEditDto);
    GroupResponseDto addSubjectToGroup(GroupSubjectAddDto groupSubjectAddDto);
    void deleteGroup(Long groupId);
}
