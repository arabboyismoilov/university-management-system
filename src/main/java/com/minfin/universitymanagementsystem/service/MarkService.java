package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.mark.MarkCreateDto;
import com.minfin.universitymanagementsystem.dto.mark.MarkEditDto;
import com.minfin.universitymanagementsystem.dto.mark.MarkResponseDto;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.List;

public interface MarkService {
    MarkResponseDto createMark(MarkCreateDto markCreateDto);
    List<MarkResponseDto> getAllMarks(int pageNumber, int pageSize);
    MarkResponseDto getMark(Long markId);
    MarkResponseDto editMark(Long markId, MarkEditDto markEditDto);
    List<MarkResponseDto> getMarksByGroupIdAndExamId(Long groupId, Long examId);
    byte[] generateFileForExamMarksForGroup(Long groupId, Long examId);
    void deleteMark(Long markId);
    void deleteMarlByExamId(Long examId);
}
