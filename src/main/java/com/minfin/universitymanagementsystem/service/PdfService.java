package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.entity.Student;

public interface PdfService {
    byte[] generateStudentReportPdf(Student student);
}
