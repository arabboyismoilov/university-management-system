package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.email.CustomEmailDto;
import com.minfin.universitymanagementsystem.dto.student.StudentCreateDto;
import com.minfin.universitymanagementsystem.dto.student.StudentEditDto;
import com.minfin.universitymanagementsystem.dto.student.StudentResponseDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface StudentService {
    StudentResponseDto createStudent(StudentCreateDto studentCreateDto);
    List<StudentResponseDto> getAllStudents(int pageNumber, int pageSize);
    StudentResponseDto getStudent(Long studentId);
    StudentResponseDto editStudent(Long studentId, StudentEditDto studentEditDto);
    List<StudentResponseDto> getStudentsByProvidedName(String providedName);
    void generateStudentReport(HttpServletResponse response, Long studentId);
    void sendEmailToStudent(CustomEmailDto customEmailDto, Long studentId);
    void sendEmailToGroupOfStudents(CustomEmailDto customEmailDto, Long groupId);
    void deleteStudent(Long studentId);
}
