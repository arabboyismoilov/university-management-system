package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.subject.SubjectCreateDto;
import com.minfin.universitymanagementsystem.dto.subject.SubjectEditDto;
import com.minfin.universitymanagementsystem.dto.subject.SubjectResponseDto;

import java.util.List;

public interface SubjectService {
    SubjectResponseDto createSubject(SubjectCreateDto subjectCreateDto);
    List<SubjectResponseDto> getAllSubjects(int pageNumber, int pageSize);
    SubjectResponseDto getSubject(Long subjectId);
    SubjectResponseDto editSubject(Long subjectId, SubjectEditDto subjectEditDto);
    List<SubjectResponseDto> getSubjectsByStudentId(Long studentId);
    List<SubjectResponseDto> getSubjectsByGroupId(Long groupId);
    void deleteSubject(Long subjectId);
}
