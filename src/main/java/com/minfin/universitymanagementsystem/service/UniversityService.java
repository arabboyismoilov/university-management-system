package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.university.UniversityCreateDto;
import com.minfin.universitymanagementsystem.dto.university.UniversityEditDto;
import com.minfin.universitymanagementsystem.dto.university.UniversityResponseDto;

import java.util.List;

public interface UniversityService {
    UniversityResponseDto createUniversity(UniversityCreateDto universityCreateDto);
    List<UniversityResponseDto> getAllUniversities(int pageNumber, int pageSize);
    UniversityResponseDto getUniversity(Long universityId);
    UniversityResponseDto editUniversity(Long universityId, UniversityEditDto universityEditDto);
    void deleteUniversity(Long universityId);
}
