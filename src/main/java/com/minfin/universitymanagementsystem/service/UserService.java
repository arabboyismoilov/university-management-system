package com.minfin.universitymanagementsystem.service;

import com.minfin.universitymanagementsystem.dto.user.UserCreateDto;
import com.minfin.universitymanagementsystem.dto.user.UserResponseDto;
import com.minfin.universitymanagementsystem.entity.User;

public interface UserService {
    User createUser(UserCreateDto userCreateDto);
}
