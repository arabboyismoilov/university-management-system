package com.minfin.universitymanagementsystem.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.minfin.universitymanagementsystem.dto.auth.LoginDto;
import com.minfin.universitymanagementsystem.dto.auth.LoginResponseDto;
import com.minfin.universitymanagementsystem.exception.BadRequestException;
import com.minfin.universitymanagementsystem.security.JwtUtil;
import com.minfin.universitymanagementsystem.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.REFRESH_TOKEN_NOT_FOUND;

/**
 * Service class that handles authentication operations.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {
    private final UserDetailsService userDetailsService;
    private final AuthenticationManager authenticationManager;

    /**
     * Retrieves an object that contains access and refresh tokens.
     *
     * @param loginDto The object that stores user credentials to login.
     * @return The object if found, otherwise throw exception.
     */
    @Override
    public LoginResponseDto login(LoginDto loginDto) {
        try {
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginDto.getEmail(),
                    loginDto.getPassword()
            ));

            UserDetails user = (UserDetails) authenticate.getPrincipal();
            String accessToken = JwtUtil.generateAccessToken(user);
            String refreshToken = JwtUtil.generateRefreshToken(user);

            log.info(String.format("User username: %s, role: %s logged in",user.getUsername(), user.getAuthorities().toString()));

            return new LoginResponseDto(accessToken, refreshToken);
        } catch (Exception exception){
            throw new BadRequestException(exception.getMessage());
        }
    }

    /**
     * Retrieves an object that contains access and refresh tokens.
     *
     * @param request Gets the request header Authorization which stores refresh token
     * @return The object if found, otherwise throw exception.
     */
    @Override
    public LoginResponseDto refreshToken(HttpServletRequest request) {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (header == null || !header.startsWith("Bearer ")){
            throw new BadRequestException(REFRESH_TOKEN_NOT_FOUND.getMessage());
        }

        try {
            String token = header.substring("Bearer ".length());

            DecodedJWT decodedJWT = JWT
                    .require(JwtUtil.getAlgorithm())
                    .build()
                    .verify(token);

            String username = decodedJWT.getSubject();
            UserDetails user = userDetailsService.loadUserByUsername(username);

            String accessToken = JwtUtil.generateAccessToken(user);
            String refreshToken = JwtUtil.generateRefreshToken(user);

            log.info(String.format("User %s refreshed its token",user.getUsername()));
            return new LoginResponseDto(accessToken, refreshToken);
        } catch (Exception e){
            throw new BadRequestException(e.getMessage());
        }
    }
}
