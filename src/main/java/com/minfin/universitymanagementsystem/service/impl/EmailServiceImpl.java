package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.email.EmailDto;
import com.minfin.universitymanagementsystem.dto.email.EmailMessage;
import com.minfin.universitymanagementsystem.exception.config.ApiRequestException;
import com.minfin.universitymanagementsystem.rabbitmq.producer.EmailSenderProducer;
import com.minfin.universitymanagementsystem.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.INTERNAL_ERROR;

/**
 * Service class that handles email sending operations.
 */
@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender javaMailSender;
    private final EmailSenderProducer emailSenderProducer;

    /**
     * Sends email that has no attachments(file, images, etc).
     *
     * @param emailDto The object that stores email message fields
     */
    @Override
    public void sendEmail(EmailDto emailDto) {
        // sending message to queue using producer
        emailSenderProducer.sendEmailToQueue(new EmailMessage(emailDto));
    }

    /**
     * Sends email that has attachments(file, images, etc).
     *
     * @param emailDto The object that stores email message fields
     */
    @Override
    public void sendEmailWithAttachment(EmailDto emailDto, String filePath) {
        emailSenderProducer.sendEmailToQueue(new EmailMessage(emailDto, filePath));
    }
}
