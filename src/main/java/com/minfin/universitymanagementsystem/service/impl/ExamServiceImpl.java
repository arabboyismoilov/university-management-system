package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.exam.ExamCreateDto;
import com.minfin.universitymanagementsystem.dto.exam.ExamEditDto;
import com.minfin.universitymanagementsystem.dto.exam.ExamResponseDto;
import com.minfin.universitymanagementsystem.entity.Exam;
import com.minfin.universitymanagementsystem.entity.Subject;
import com.minfin.universitymanagementsystem.exception.ConflictException;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.mapper.ExamMapper;
import com.minfin.universitymanagementsystem.repository.ExamRepository;
import com.minfin.universitymanagementsystem.repository.SubjectRepository;
import com.minfin.universitymanagementsystem.service.ExamService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.*;

/**
 * Service class that handles exam entity CRUD operations.
 */
@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class ExamServiceImpl implements ExamService {
    private final ExamRepository examRepository;
    private final SubjectRepository subjectRepository;
    private final ExamMapper examMapper;

    @Override
    public ExamResponseDto createExam(ExamCreateDto examCreateDto) {
        Long subjectId = examCreateDto.getSubjectId();
        String name = examCreateDto.getName();

        Subject subject = subjectRepository.findById(subjectId).orElseThrow(
                () -> new NotFoundException(String.format(SUBJECT_NOT_FOUND.getMessage(), subjectId))
        );

        boolean existsByNameAndSubjectId = examRepository.existsByNameAndSubject_Id(name, subjectId);
        if (existsByNameAndSubjectId){
            throw new ConflictException(String.format(EXAM_ALREADY_EXIST.getMessage(), name, subjectId));
        }

        Exam exam = new Exam(name, subject);
        Exam saved = examRepository.save(exam);

        log.info(String.format("Exam with ID [%s] created", saved.getId()));
        return examMapper.responseDto(saved);
    }

    @Override
    public List<ExamResponseDto> getAllExams(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return examRepository.findAll(pageable).stream()
                .map(examMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public List<ExamResponseDto> getExamsBySubjectId(Long subjectId) {
        Subject subject = subjectRepository.findById(subjectId).orElseThrow(
                () -> new NotFoundException(String.format(SUBJECT_NOT_FOUND.getMessage(), subjectId))
        );
        return subject.getExams().stream()
                .map(examMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public ExamResponseDto getExam(Long examId) {
        Exam exam = examRepository.findById(examId).orElseThrow(
                () -> new NotFoundException(String.format(EXAM_NOT_FOUND.getMessage(), examId))
        );
        return examMapper.responseDto(exam);
    }

    @Override
    public ExamResponseDto editExam(Long examId, ExamEditDto examEditDto) {
        Exam exam = examRepository.findById(examId).orElseThrow(
                () -> new NotFoundException(String.format(EXAM_NOT_FOUND.getMessage(), examId))
        );

        examMapper.editExam(exam, examEditDto);
        Exam edited = examRepository.save(exam);

        log.info(String.format("Exam with ID [%s] edited", edited.getId()));
        return examMapper.responseDto(edited);
    }

    @Override
    public void deleteExam(Long examId) {
        boolean existsById = examRepository.existsById(examId);
        if (!existsById){
            throw new NotFoundException(String.format(EXAM_NOT_FOUND.getMessage(), examId));
        }

        examRepository.deleteById(examId);
        log.info(String.format("Exam with ID [%s] deleted", examId));
    }
}
