package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.mark.MarkResponseDto;
import com.minfin.universitymanagementsystem.exception.config.ApiRequestException;
import com.minfin.universitymanagementsystem.service.ExcelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.INTERNAL_ERROR;

/**
 * Service class that handles excel file generating operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ExcelServiceImpl implements ExcelService {
    @Override
    public byte[] generateExamMarksForGroup(List<MarkResponseDto> marks) {


        // Create Excel workbook and sheet
        try(Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            Sheet sheet = workbook.createSheet("Marks");

            // Create header row
            Row headerRow = sheet.createRow(0);
            headerRow.createCell(0).setCellValue("Student ID");
            headerRow.createCell(1).setCellValue("Name");
            headerRow.createCell(2).setCellValue("Subject");
            headerRow.createCell(3).setCellValue("Exam");
            headerRow.createCell(4).setCellValue("Mark");

            // Create data rows
            int rowNum = 1;
            for (MarkResponseDto mark : marks) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(mark.getStudentId());
                row.createCell(1).setCellValue(mark.getStudentName());
                row.createCell(2).setCellValue(mark.getSubjectName());
                row.createCell(3).setCellValue(mark.getExamName());
                row.createCell(4).setCellValue(mark.getValue());
            }

            workbook.write(byteArrayOutputStream);

            log.info("Excel file created for marks");
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new ApiRequestException(INTERNAL_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
