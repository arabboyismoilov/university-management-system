package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.faculty.FacultyCreateDto;
import com.minfin.universitymanagementsystem.dto.faculty.FacultyEditDto;
import com.minfin.universitymanagementsystem.dto.faculty.FacultyResponseDto;
import com.minfin.universitymanagementsystem.entity.Faculty;
import com.minfin.universitymanagementsystem.entity.University;
import com.minfin.universitymanagementsystem.exception.ConflictException;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.mapper.FacultyMapper;
import com.minfin.universitymanagementsystem.repository.FacultyRepository;
import com.minfin.universitymanagementsystem.repository.UniversityRepository;
import com.minfin.universitymanagementsystem.service.FacultyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.*;

/**
 * Service class that handles faculty entity CRUD operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class FacultyServiceImpl implements FacultyService {
    private final FacultyRepository facultyRepository;
    private final UniversityRepository universityRepository;
    private final FacultyMapper facultyMapper;

    @Override
    public FacultyResponseDto createFaculty(FacultyCreateDto facultyCreateDto) {
        String name = facultyCreateDto.getName();
        Long universityId = facultyCreateDto.getUniversityId();

        University university = universityRepository.findById(universityId).orElseThrow(
                () -> new NotFoundException(String.format(UNIVERSITY_NOT_FOUND.getMessage(), universityId))
        );

        boolean existsByNameAndUniversity_id = facultyRepository.existsByNameAndUniversity_Id(name, universityId);
        if (existsByNameAndUniversity_id){
            throw new ConflictException(String.format(FACULTY_ALREADY_EXIST_IN_THIS_UNIVERSITY.getMessage(), name, universityId));
        }

        Faculty faculty = new Faculty(name, new ArrayList<>(), university);
        Faculty saved = facultyRepository.save(faculty);
        log.info(String.format("Faculty with ID [%s] created", saved.getId()));

        return facultyMapper.responseDto(saved);
    }

    @Override
    public List<FacultyResponseDto> getAllFaculties(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return facultyRepository.findAll(pageable).stream()
                .map(facultyMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public FacultyResponseDto getFaculty(Long facultyId) {
        Faculty faculty = facultyRepository.findById(facultyId).orElseThrow(
                () -> new NotFoundException(String.format(FACULTY_NOT_FOUND.getMessage(), facultyId))
        );
        return facultyMapper.responseDto(faculty);
    }

    @Override
    public FacultyResponseDto editFaculty(Long facultyId, FacultyEditDto facultyEditDto) {
        Faculty faculty = facultyRepository.findById(facultyId).orElseThrow(
                () -> new NotFoundException(String.format(FACULTY_NOT_FOUND.getMessage(), facultyId))
        );

        facultyMapper.editFaculty(faculty, facultyEditDto);
        Faculty edited = facultyRepository.save(faculty);
        log.info(String.format("Faculty with ID [%s] edited", facultyId));

        return facultyMapper.responseDto(edited);
    }

    @Override
    public void deleteFaculty(Long facultyId) {
        boolean existsById = facultyRepository.existsById(facultyId);
        if (!existsById){
            throw new NotFoundException(String.format(FACULTY_NOT_FOUND.getMessage(), facultyId));
        }

        facultyRepository.deleteById(facultyId);
        log.info(String.format("Faculty with ID [%s] deleted", facultyId));
    }
}
