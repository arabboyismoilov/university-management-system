package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.group.GroupCreateDto;
import com.minfin.universitymanagementsystem.dto.group.GroupEditDto;
import com.minfin.universitymanagementsystem.dto.group.GroupResponseDto;
import com.minfin.universitymanagementsystem.dto.group.GroupSubjectAddDto;
import com.minfin.universitymanagementsystem.entity.Faculty;
import com.minfin.universitymanagementsystem.entity.Group;
import com.minfin.universitymanagementsystem.entity.Subject;
import com.minfin.universitymanagementsystem.exception.ConflictException;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.mapper.GroupMapper;
import com.minfin.universitymanagementsystem.repository.FacultyRepository;
import com.minfin.universitymanagementsystem.repository.GroupRepository;
import com.minfin.universitymanagementsystem.repository.SubjectRepository;
import com.minfin.universitymanagementsystem.service.GroupService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.*;

/**
 * Service class that handles group entity CRUD operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;
    private final FacultyRepository facultyRepository;
    private final SubjectRepository subjectRepository;
    private final GroupMapper groupMapper;

    @Override
    public GroupResponseDto createGroup(GroupCreateDto groupCreateDto) {
        Long facultyId = groupCreateDto.getFacultyId();
        String name = groupCreateDto.getName();

        Faculty faculty = facultyRepository.findById(facultyId).orElseThrow(
                () -> new NotFoundException(String.format(FACULTY_NOT_FOUND.getMessage(), facultyId))
        );

        boolean existsByNameAndFacultyId = groupRepository.existsByNameAndFaculty_Id(name, facultyId);
        if (existsByNameAndFacultyId){
            throw new ConflictException(String.format(GROUP_ALREADY_EXIST_IN_THIS_FACULTY.getMessage(), name, faculty.getName()));
        }

        Group group = new Group(name, groupCreateDto.getOpenedYear(), faculty, new ArrayList<>(), new ArrayList<>());
        Group saved = groupRepository.save(group);
        log.info(String.format("Group with ID [%s] created", saved.getId()));

        return groupMapper.responseDto(saved);
    }

    @Override
    public List<GroupResponseDto> getAllGroups(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return groupRepository.findAll(pageable).stream()
                .map(groupMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public GroupResponseDto getGroup(Long groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
        );
        return groupMapper.responseDto(group);
    }

    @Override
    public List<GroupResponseDto> getGroupsByFacultyId(Long facultyId) {
        Faculty faculty = facultyRepository.findById(facultyId).orElseThrow(
                () -> new NotFoundException(String.format(FACULTY_NOT_FOUND.getMessage(), facultyId))
        );

        return groupRepository.findAllByFaculty(faculty).stream()
                .map(groupMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public GroupResponseDto editGroup(Long groupId, GroupEditDto groupEditDto) {
        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
        );

        groupMapper.editGroup(group, groupEditDto);
        Group edited = groupRepository.save(group);
        log.info(String.format("Group with ID [%s] edited", groupId));

        return groupMapper.responseDto(edited);
    }

    @Override
    public GroupResponseDto addSubjectToGroup(GroupSubjectAddDto groupSubjectAddDto) {
        Long subjectId = groupSubjectAddDto.getSubjectId();
        Long groupId = groupSubjectAddDto.getGroupId();

        Subject subject = subjectRepository.findById(subjectId).orElseThrow(
                () -> new NotFoundException(String.format(SUBJECT_NOT_FOUND.getMessage(), subjectId))
        );

        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
        );

        List<Subject> subjects = group.getSubjects();
        Optional<Subject> optionalSubject = subjects.stream().filter(item -> item.getId() == subjectId).findFirst();
        if (optionalSubject.isPresent()){
            throw new ConflictException(String.format(SUBJECT_ALREADY_EXIST_IN_THIS_GROUP.getMessage(), subjectId, groupId));
        }

        subjects.add(subject);
        Group edited = groupRepository.save(group);
        log.info(String.format("Subject with ID [%s] added to Group with ID [%s]", subjectId, groupId));

        return groupMapper.responseDto(edited);
    }

    @Override
    public void deleteGroup(Long groupId) {
        boolean existsById = groupRepository.existsById(groupId);
        if (!existsById){
            throw new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId));
        }

        groupRepository.deleteById(groupId);
        log.info(String.format("Group with ID [%s] deleted", groupId));
    }
}
