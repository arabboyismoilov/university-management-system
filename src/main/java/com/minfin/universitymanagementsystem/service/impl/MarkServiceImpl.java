package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.mark.MarkEditDto;
import com.minfin.universitymanagementsystem.dto.mark.MarkResponseDto;
import com.minfin.universitymanagementsystem.entity.*;
import com.minfin.universitymanagementsystem.exception.ConflictException;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.exception.config.ApiRequestException;
import com.minfin.universitymanagementsystem.mapper.MarkMapper;
import com.minfin.universitymanagementsystem.repository.*;
import com.minfin.universitymanagementsystem.dto.mark.MarkCreateDto;
import com.minfin.universitymanagementsystem.service.ExcelService;
import com.minfin.universitymanagementsystem.service.MarkService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.*;

/**
 * Service class that handles mark entity CRUD operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class MarkServiceImpl implements MarkService {
    private final MarkRepository markRepository;
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final ExamRepository examRepository;
    private final MarkMapper markMapper;
    private final ExcelService excelService;

    @Override
    public MarkResponseDto createMark(MarkCreateDto markCreateDto) {
        Long examId = markCreateDto.getExamId();
        Long studentId = markCreateDto.getStudentId();

        Student student = studentRepository.findById(studentId).orElseThrow(
                () -> new NotFoundException(String.format(STUDENT_NOT_FOUND.getMessage(), studentId))
        );

        Exam exam = examRepository.findById(examId).orElseThrow(
                () -> new NotFoundException(String.format(EXAM_NOT_FOUND.getMessage(), examId))
        );

        boolean existsByStudentAndExam = markRepository.existsByStudentAndExam(student, exam);
        if (existsByStudentAndExam){
            throw new ConflictException(String.format(MARK_ALREADY_EXIST.getMessage(), studentId, examId));
        }

        Mark mark = new Mark(markCreateDto.getValue(), student, exam.getSubject(), exam);
        Mark saved = markRepository.save(mark);
        log.info(String.format("Mark with ID [%s] for student ID [%s] for exam ID[%s] created", saved.getId(), studentId, examId));

        return markMapper.responseDto(saved);
    }

    @Override
    public List<MarkResponseDto> getAllMarks(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return markRepository.findAll(pageable).stream()
                .map(markMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public MarkResponseDto getMark(Long markId) {
        Mark mark = markRepository.findById(markId).orElseThrow(
                () -> new NotFoundException(String.format(MARK_NOT_FOUND.getMessage(), markId))
        );
        return markMapper.responseDto(mark);
    }

    @Override
    public MarkResponseDto editMark(Long markId, MarkEditDto markEditDto) {
        Mark mark = markRepository.findById(markId).orElseThrow(
                () -> new NotFoundException(String.format(MARK_NOT_FOUND.getMessage(), markId))
        );

        markMapper.editMark(mark, markEditDto);
        Mark edited = markRepository.save(mark);
        log.info(String.format("Mark with ID [%s] edited", markId));

        return markMapper.responseDto(edited);
    }

    @Override
    public List<MarkResponseDto> getMarksByGroupIdAndExamId(Long groupId, Long examId) {
        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
        );

        Exam exam = examRepository.findById(examId).orElseThrow(
                () -> new NotFoundException(String.format(EXAM_NOT_FOUND.getMessage(), examId))
        );

        return markRepository.findAllByStudent_GroupAndExamOrderByValueDesc(group, exam)
                .stream().map(markMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public byte[] generateFileForExamMarksForGroup(Long groupId, Long examId) {
        List<MarkResponseDto> marks = getMarksByGroupIdAndExamId(groupId, examId);
        return excelService.generateExamMarksForGroup(marks);
    }

    @Override
    public void deleteMark(Long markId) {
        boolean existsById = markRepository.existsById(markId);
        if (!existsById){
            throw new NotFoundException(String.format(MARK_NOT_FOUND.getMessage(), markId));
        }

        markRepository.deleteById(markId);
        log.info(String.format("Mark with ID [%s] deleted", markId));
    }

    @Override
    public void deleteMarlByExamId(Long examId) {
        Exam exam = examRepository.findById(examId).orElseThrow(
                () -> new NotFoundException(String.format(EXAM_NOT_FOUND.getMessage(), examId))
        );

        markRepository.deleteAllByExam(exam);
        log.info(String.format("Marks with exam ID [%s] are deleted", examId));
    }
}
