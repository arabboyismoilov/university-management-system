package com.minfin.universitymanagementsystem.service.impl;

import com.lowagie.text.DocumentException;
import com.minfin.universitymanagementsystem.entity.Student;
import com.minfin.universitymanagementsystem.exception.config.ApiRequestException;
import com.minfin.universitymanagementsystem.service.PdfService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.INTERNAL_ERROR;

/**
 * Service class that handles pdf generating operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PdfServiceImpl implements PdfService {
    private final SpringTemplateEngine templateEngine;

    @Override
    public byte[] generateStudentReportPdf(Student student) {
        // Create Thymeleaf context
        Context context = new Context();
        context.setVariable("name", student.getName());
        context.setVariable("universityName", student.getGroup().getFaculty().getUniversity().getName());
        context.setVariable("academicYear", student.getAcademicYear());
        context.setVariable("facultyName", student.getGroup().getFaculty().getName());
        context.setVariable("groupName", student.getGroup().getName());

        String pattern = "yyyy-MM-dd HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        String date = LocalDateTime.now().format(formatter);

        context.setVariable("date", date);

        // Render the Thymeleaf template as HTML
        String htmlContent = templateEngine.process("student-report", context);

        try (PDDocument document = new PDDocument(); ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            PDPage page = new PDPage();
            document.addPage(page);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(htmlContent  );

            renderer.layout();
            renderer.createPDF(outputStream);
            renderer.finishPDF();

            log.info(String.format("Student Report pdf file for Stundet with ID [%s] created", student.getId()));
            return outputStream.toByteArray();
        } catch (IOException | DocumentException e) {
            throw new ApiRequestException(INTERNAL_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
