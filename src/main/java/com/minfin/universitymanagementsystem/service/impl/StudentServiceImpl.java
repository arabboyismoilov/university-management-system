package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.email.CustomEmailDto;
import com.minfin.universitymanagementsystem.dto.email.EmailDto;
import com.minfin.universitymanagementsystem.dto.student.StudentCreateDto;
import com.minfin.universitymanagementsystem.dto.student.StudentEditDto;
import com.minfin.universitymanagementsystem.dto.student.StudentResponseDto;
import com.minfin.universitymanagementsystem.dto.user.UserCreateDto;
import com.minfin.universitymanagementsystem.entity.Group;
import com.minfin.universitymanagementsystem.entity.Student;
import com.minfin.universitymanagementsystem.entity.User;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.exception.config.ApiRequestException;
import com.minfin.universitymanagementsystem.mapper.StudentMapper;
import com.minfin.universitymanagementsystem.repository.GroupRepository;
import com.minfin.universitymanagementsystem.repository.StudentRepository;
import com.minfin.universitymanagementsystem.service.EmailService;
import com.minfin.universitymanagementsystem.service.PdfService;
import com.minfin.universitymanagementsystem.service.StudentService;
import com.minfin.universitymanagementsystem.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.minfin.universitymanagementsystem.enums.EmailMessages.STUDENT_CREATED_MESSAGE;
import static com.minfin.universitymanagementsystem.enums.EmailMessages.SUCCESS_REGISTRATION_SUBJECT;
import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.*;

/**
 * Service class that handles student entity CRUD operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final StudentMapper studentMapper;
    private final UserService userService;
    private final PdfService pdfService;
    private final EmailService emailService;

    /**
     * Retrieves an object that contains student entity info.
     *
     * @param studentCreateDto The object that stores student creation data.
     * @return The object if succeeded, otherwise throw exception.
     * after creating student, it will send en email message to student's email about successful
     * account creation and user credentials to enter system
     */
    @Override
    public StudentResponseDto createStudent(StudentCreateDto studentCreateDto) {
        String email = studentCreateDto.getEmail();
        Long groupId = studentCreateDto.getGroupId();

        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
        );

        User user = userService.createUser(
                new UserCreateDto(email, studentCreateDto.getPassword(),
                        studentCreateDto.getName(), "ROLE_STUDENT",
                        studentCreateDto.getPhoneNumber())
        );

        Student student = new Student(
                studentCreateDto.getName(), studentCreateDto.getAddress(),
                studentCreateDto.getBirthDate(), studentCreateDto.getEnteredYear(),
                studentCreateDto.getAcademicYear(), user, group
        );

        Student saved = studentRepository.save(student);
        log.info(String.format("Student with ID [%s] created", saved.getId()));

        //sending email verification to user
        String universityName = group.getFaculty().getUniversity().getName();
        emailService.sendEmail(new EmailDto(
                email, SUCCESS_REGISTRATION_SUBJECT.getMessage(),
                String.format(STUDENT_CREATED_MESSAGE.getMessage(), universityName, studentCreateDto.getPassword(), email)
        ));
        log.info(String.format("Email notification about account successful creation is sent to student with ID [%s]", saved.getId()));

        return studentMapper.responseDto(saved);
    }

    @Override
    public List<StudentResponseDto> getAllStudents(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return studentRepository.findAll(pageable).stream()
                .map(studentMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public StudentResponseDto getStudent(Long studentId) {
        Student student = studentRepository.findById(studentId).orElseThrow(
                () -> new NotFoundException(String.format(STUDENT_NOT_FOUND.getMessage(), studentId))
        );
        return studentMapper.responseDto(student);
    }

    @Override
    public StudentResponseDto editStudent(Long studentId, StudentEditDto studentEditDto) {
        Student student = studentRepository.findById(studentId).orElseThrow(
                () -> new NotFoundException(String.format(STUDENT_NOT_FOUND.getMessage(), studentId))
        );

        studentMapper.editStudent(student, studentEditDto);

        Long groupId = studentEditDto.getGroupId();
        if (groupId != null){
            Group group = groupRepository.findById(groupId).orElseThrow(
                    () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
            );

            student.setGroup(group);
        }

        Student edited = studentRepository.save(student);
        log.info(String.format("Student with ID [%s] edited", studentId));
        return studentMapper.responseDto(edited);
    }

    @Override
    public List<StudentResponseDto> getStudentsByProvidedName(String providedName) {
        return studentRepository.findByNameContainingIgnoreCase(providedName).stream()
                .map(studentMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public void generateStudentReport(HttpServletResponse response, Long studentId) {
        Student student = studentRepository.findById(studentId).orElseThrow(
                () -> new NotFoundException(String.format(STUDENT_NOT_FOUND.getMessage(), studentId))
        );

        byte[] data = pdfService.generateStudentReportPdf(student);

        // Set content type and disposition for the response
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "attachment; filename=example.pdf");

        // Write the PDF to the response output stream
        try {
            response.getOutputStream().write(data);
            response.getOutputStream().flush();
        } catch (IOException e) {
            throw new ApiRequestException(INTERNAL_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    @Override
    public void sendEmailToStudent(CustomEmailDto customEmailDto, Long studentId) {
        Student student = studentRepository.findById(studentId).orElseThrow(
                () -> new NotFoundException(String.format(STUDENT_NOT_FOUND.getMessage(), studentId))
        );

        EmailDto emailDto = new EmailDto(student.getUser().getEmail(),
                customEmailDto.getSubject(), customEmailDto.getBody());
        emailService.sendEmail(emailDto);
        log.info(String.format("Email is sent to Student with ID [%s]", studentId));
    }

    @Override
    public void sendEmailToGroupOfStudents(CustomEmailDto customEmailDto, Long groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
        );

        List<EmailDto> emailDtoList = group.getStudents().stream()
                .map(student -> new EmailDto(student.getUser().getEmail(), customEmailDto.getSubject(), customEmailDto.getBody()))
                .collect(Collectors.toList());

        emailDtoList.forEach(emailService::sendEmail);
        log.info(String.format("Emails are sent to student with Group ID [%s]", groupId));
    }

    @Override
    public void deleteStudent(Long studentId) {
        boolean existsById = studentRepository.existsById(studentId);
        if (!existsById){
            throw new NotFoundException(String.format(STUDENT_NOT_FOUND.getMessage(), studentId));
        }

        studentRepository.deleteById(studentId);
        log.info(String.format("Student with ID [%s] deleted", studentId));
    }
}
