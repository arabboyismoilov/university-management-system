package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.subject.SubjectCreateDto;
import com.minfin.universitymanagementsystem.dto.subject.SubjectEditDto;
import com.minfin.universitymanagementsystem.dto.subject.SubjectResponseDto;
import com.minfin.universitymanagementsystem.entity.Group;
import com.minfin.universitymanagementsystem.entity.Student;
import com.minfin.universitymanagementsystem.entity.Subject;
import com.minfin.universitymanagementsystem.entity.University;
import com.minfin.universitymanagementsystem.exception.ConflictException;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.mapper.SubjectMapper;
import com.minfin.universitymanagementsystem.repository.GroupRepository;
import com.minfin.universitymanagementsystem.repository.StudentRepository;
import com.minfin.universitymanagementsystem.repository.SubjectRepository;
import com.minfin.universitymanagementsystem.repository.UniversityRepository;
import com.minfin.universitymanagementsystem.service.SubjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.*;

/**
 * Service class that handles subject entity CRUD operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;
    private final UniversityRepository universityRepository;
    private final StudentRepository studentRepository;
    private final SubjectMapper subjectMapper;
    private final GroupRepository groupRepository;

    @Override
    public SubjectResponseDto createSubject(SubjectCreateDto subjectCreateDto) {
        String name = subjectCreateDto.getName();
        Long universityId = subjectCreateDto.getUniversityId();

        University university = universityRepository.findById(universityId).orElseThrow(
                () -> new NotFoundException(String.format(USER_NOT_FOUND.getMessage(), universityId))
        );

        boolean existsByName = subjectRepository.existsByNameAndUniversity_Id(name, universityId);
        if (existsByName){
            throw new ConflictException(String.format(SUBJECT_ALREADY_EXIST.getMessage(), name, university));
        }

        Subject subject = new Subject(name, subjectCreateDto.getHours(), new ArrayList<>(), new ArrayList<>(), university);
        Subject saved = subjectRepository.save(subject);
        log.info(String.format("Subject with ID [%s] created", saved.getId()));

        return subjectMapper.responseDto(saved);
    }

    @Override
    public List<SubjectResponseDto> getAllSubjects(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return subjectRepository.findAll(pageable).stream()
                .map(subjectMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public SubjectResponseDto getSubject(Long subjectId) {
        Subject subject = subjectRepository.findById(subjectId).orElseThrow(
                () -> new NotFoundException(String.format(SUBJECT_NOT_FOUND.getMessage(), subjectId))
        );
        return subjectMapper.responseDto(subject);
    }

    @Override
    public SubjectResponseDto editSubject(Long subjectId, SubjectEditDto subjectEditDto) {
        Subject subject = subjectRepository.findById(subjectId).orElseThrow(
                () -> new NotFoundException(String.format(SUBJECT_NOT_FOUND.getMessage(), subjectId))
        );
        subjectMapper.editSubject(subject, subjectEditDto);
        Subject edited = subjectRepository.save(subject);
        log.info(String.format("Subject with ID [%s] edited", subjectId));

        return subjectMapper.responseDto(edited);
    }

    @Override
    public List<SubjectResponseDto> getSubjectsByStudentId(Long studentId) {
        Student student = studentRepository.findById(studentId).orElseThrow(
                () -> new NotFoundException(String.format(STUDENT_NOT_FOUND.getMessage(), studentId))
        );

        return student.getGroup().getSubjects().stream()
                .map(subjectMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public List<SubjectResponseDto> getSubjectsByGroupId(Long groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow(
                () -> new NotFoundException(String.format(GROUP_NOT_FOUND.getMessage(), groupId))
        );
        return group.getSubjects().stream()
                .map(subjectMapper::responseDto).collect(Collectors.toList());
    }

    @Override
    public void deleteSubject(Long subjectId) {
        boolean existsById = subjectRepository.existsById(subjectId);
        if (!existsById){
            throw new NotFoundException(String.format(SUBJECT_NOT_FOUND.getMessage(), subjectId));
        }

        subjectRepository.deleteById(subjectId);
        log.info(String.format("Subject with ID [%s] deleted", subjectId));
    }
}
