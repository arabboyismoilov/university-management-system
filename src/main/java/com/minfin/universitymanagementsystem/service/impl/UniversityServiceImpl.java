package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.university.UniversityCreateDto;
import com.minfin.universitymanagementsystem.dto.university.UniversityEditDto;
import com.minfin.universitymanagementsystem.dto.university.UniversityResponseDto;
import com.minfin.universitymanagementsystem.entity.University;
import com.minfin.universitymanagementsystem.exception.ConflictException;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.mapper.UniversityMapper;
import com.minfin.universitymanagementsystem.repository.UniversityRepository;
import com.minfin.universitymanagementsystem.service.UniversityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.*;

/**
 * Service class that handles university entity CRUD operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class UniversityServiceImpl implements UniversityService {
    private final UniversityRepository universityRepository;
    private final UniversityMapper universityMapper;

    @Override
    public UniversityResponseDto createUniversity(UniversityCreateDto universityCreateDto) {
        String name = universityCreateDto.getName();

        boolean existsByName = universityRepository.existsByName(name);
        if (existsByName){
            throw new ConflictException(String.format(UNIVERSITY_NAME_ALREADY_EXIST.getMessage(), name));
        }

        University university = new University(name, universityCreateDto.getAddress(), universityCreateDto.getOpenedYear(), new ArrayList<>(), new ArrayList<>());
        University saved = universityRepository.save(university);
        log.info(String.format("University with ID [%s] created", saved.getId()));

        return universityMapper.responseDto(saved);
    }

    @Override
    public List<UniversityResponseDto> getAllUniversities(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return universityRepository.findAll(pageable)
                .stream()
                .map(universityMapper::responseDto)
                .collect(Collectors.toList());
    }

    @Override
    public UniversityResponseDto getUniversity(Long universityId) {
        University university = universityRepository.findById(universityId).orElseThrow(
                () -> new NotFoundException(String.format(UNIVERSITY_NOT_FOUND.getMessage(), universityId))
        );
        return universityMapper.responseDto(university);
    }

    @Override
    public UniversityResponseDto editUniversity(Long universityId, UniversityEditDto universityEditDto) {
        University university = universityRepository.findById(universityId).orElseThrow(
                () -> new NotFoundException(String.format(UNIVERSITY_NOT_FOUND.getMessage(), universityId))
        );

        universityMapper.editUniversity(university, universityEditDto);
        University edited = universityRepository.save(university);
        log.info(String.format("University with ID [%s] edited", universityId));

        return universityMapper.responseDto(edited);
    }

    @Override
    public void deleteUniversity(Long universityId) {
        boolean existsById = universityRepository.existsById(universityId);
        if (!existsById){
            throw new NotFoundException(String.format(UNIVERSITY_NOT_FOUND.getMessage(), universityId));
        }

        universityRepository.deleteById(universityId);
        log.info(String.format("University with ID [%s] deleted", universityId));
    }
}
