package com.minfin.universitymanagementsystem.service.impl;

import com.minfin.universitymanagementsystem.dto.user.UserCreateDto;
import com.minfin.universitymanagementsystem.entity.Role;
import com.minfin.universitymanagementsystem.entity.User;
import com.minfin.universitymanagementsystem.exception.ConflictException;
import com.minfin.universitymanagementsystem.exception.NotFoundException;
import com.minfin.universitymanagementsystem.repository.RoleRepository;
import com.minfin.universitymanagementsystem.repository.UserRepository;
import com.minfin.universitymanagementsystem.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.EMAIL_ALREADY_REGISTERED;
import static com.minfin.universitymanagementsystem.enums.ExceptionMessages.ROLE_NOT_FOUND;

/**
 * Service class that handles user entity operations.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public User createUser(UserCreateDto userCreateDto) {
        String email = userCreateDto.getEmail();
        String roleName = userCreateDto.getRoleName();

        boolean isExistEmail = userRepository.existsByEmail(email);
        if (isExistEmail){
            throw new ConflictException(String.format(EMAIL_ALREADY_REGISTERED.getMessage(), email));
        }

        Role role = roleRepository.findByName(roleName).orElseThrow(
                () -> new NotFoundException(String.format(ROLE_NOT_FOUND.getMessage(), roleName))
        );

        User user = new User(
                userCreateDto.getName(), email,
                passwordEncoder.encode(userCreateDto.getPassword()),
                userCreateDto.getPhoneNumber(), true, List.of(role)
        );

        User saved = userRepository.save(user);
        log.info(String.format("User with ID [%s], role [%s] created", saved.getId(), user.getRoles().toString()));

        return saved;
    }
}
